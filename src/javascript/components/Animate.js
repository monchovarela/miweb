export const anim = (arr, callback) =>
  (function show(n) {
    setTimeout(() => {
      n++;
      if (arr.length > n) show(n);
      return callback(n - 1);
    }, 50);
  })(0);
