export const colors = {
    simple: ['#000000', '#FFFFFF', '#888888'],
    vine: ['#A4193D', '#FFDFB9', '#3333333'],
    green: ['#2BAE66', '#FCF6F5', '#3333333'],
    red: ['#990011', '#FCF6F5', '#3333333']
  };

