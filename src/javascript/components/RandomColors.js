export const randomColors = () => {
  // create array colors names
  let arr = [];
  for (let color in colors) {
    arr.push(color);
  }
  // random array
  let rand = arr[Math.floor(Math.random() * arr.length)],
    color1 = colors[rand][0],
    color2 = colors[rand][1],
    color3 = colors[rand][2];
  console.log(`Name of color is ${rand}`);
  // set property
  document.documentElement.style.setProperty('--dark', color1);
  document.documentElement.style.setProperty('--light', color2);
  document.documentElement.style.setProperty('--txt', color3);
};
