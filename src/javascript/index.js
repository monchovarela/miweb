import { createLightbox } from './components/Lightbox';
import { anim } from './components/Animate';

const _ = (el) => document.querySelector(el);

const toogleNavigation = () => {
  _('.header').classList.toggle('active');
  _('.content').classList.toggle('active');
};

document.addEventListener('DOMContentLoaded', () => {
  _('.btn-menu').addEventListener('click', toogleNavigation);
  _('.btn-close').addEventListener('click', toogleNavigation);

  // esperamos que acabe el efecto clip-path
  let w = setTimeout(() => {
    if (_('.proyecto')) {
      let elem = document.querySelectorAll('.proyecto');
      anim(elem, (n) => elem[n].classList.add('show') );
    }
  
    let elem = document.querySelectorAll('.content p');
    anim(elem, (n) => elem[n].classList.add('show') );

    clearTimeout(w);
  },500)

  if (_('.animated-logo')) {
    document.addEventListener('mousemove', function (e) {
      const x = e.pageX / (window.innerWidth * 10),
        y = e.pageY / (window.innerHeight * 10);

      if (y <= parseFloat(0.03)) {
        if (x >= parseFloat(0.05)) {
          _('.animated-logo').className = 'animated-logo go-top-right';
        }
        if (x <= parseFloat(0.05)) {
          _('.animated-logo').className = 'animated-logo go-top-left';
        }
      } else if (y >= parseFloat(0.05)) {
        if (x >= parseFloat(0.05)) {
          _('.animated-logo').className = 'animated-logo go-down-left';
        }
        if (x <= parseFloat(0.05)) {
          _('.animated-logo').className = 'animated-logo go-down-right';
        }
      } else {
        _('.animated-logo').className = 'animated-logo';
      }
    });
  }

  //randomColors();

  // simple lightbox
  let img = document.querySelectorAll('.zoom');
  Array.from(img).map((item) => handleImages(item));
});

// handle images
function handleImages(item, index) {
  item.addEventListener('click', (evt) => {
    createLightbox({
      src: evt.target.src,
      alt: evt.target.alt ? evt.target.alt : 'Unsplash'
    });
  });
}
