import gulp from "gulp";
// Optimizar imagenes
import imagemin from "gulp-imagemin";
// Convertir imagenes a webp
import webp from "gulp-webp";
import extReplace from "gulp-ext-replace";
// Crear sourcemaps para css y js
import sourcemaps from "gulp-sourcemaps";
// Necesario para compilar javascript moderno
import rollup from "rollup-stream";
import buffer from "vinyl-buffer";
import source from "vinyl-source-stream";
import minify from "gulp-minify";
// Necesario para compilar Sass
import autoprefixer from "gulp-autoprefixer";
import csso from "gulp-csso";
import clean from "gulp-clean";
import dartSass from "sass";
import gulpSass from "gulp-sass";
// Config
import data from "gulp-data";
import yaml from "gulp-yaml";
import fs from "fs";
// Nunjucks
import render from "gulp-nunjucks-render";
// Htmlmin
import htmlmin from "gulp-htmlmin";
// Browser Sync
import browserSync from "browser-sync";

browserSync.create();
const sass = gulpSass(dartSass);

// Rutas
const src = "./src";
const dist = "./public";

/**
 * Name: clean
 * Desc: Clean public folder
 */
gulp.task("clean", () =>
  gulp
    .src([dist], {
      read: true,
      allowEmpty: true,
    })
    .pipe(
      clean({
        force: true,
      })
    )
);

/**
 * Name: cleanJson
 * Desc: Clean json file on public task
 */
gulp.task("cleanJson", () =>
  gulp
    .src(`${src}/config/data.json`, { read: true, allowEmpty: true })
    .pipe(clean({ force: true }))
);

/**
 * Name: sass
 * Desc: prefix,minify and create sourcemaps
 */
gulp.task("sass", () =>
  gulp
    .src(`${src}/sass/*.scss`)
    .pipe(sass())
    .pipe(autoprefixer())
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(csso())
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest(`${dist}/css`))
    .pipe(
      browserSync.reload({
        stream: true,
      })
    )
);

/**
 * Name: javascript
 * Desc: Minify and create sourcemaps
 */
gulp.task("javascript", () =>
  rollup({
    input: `${src}/javascript/index.js`,
    format: "iife",
    sourcemap: true,
  })
    .pipe(source("main.js"))
    .pipe(buffer())
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(minify({ ext: { min: ".min.js" } }))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest(`${dist}/javascript/`))
    .pipe(
      browserSync.reload({
        stream: true,
      })
    )
);

/**
 * Name: copyHtaccess
 * Desc: Copy .htaccess
 */
gulp.task("copyHtaccess", () =>
  gulp.src(`${src}/.htaccess`).pipe(gulp.dest(dist))
);

/**
 * Name: copyRobots
 * Desc: Copy robots.txt
 */
gulp.task("copyRobots", () =>
  gulp.src(`${src}/robots.txt`).pipe(gulp.dest(dist))
);

/**
 * Name: copyHumans
 * Desc: Copy humans.txt
 */
gulp.task("copyHumans", () =>
  gulp.src(`${src}/humans.txt`).pipe(gulp.dest(dist))
);

/**
 * Name: copyIcons
 * Desc: Copy icons folder
 */
gulp.task("copyIcons", () =>
  gulp.src(`${src}/icons/**/*`).pipe(gulp.dest(`${dist}/icons/`))
);

/**
 * Name: convertToWebp
 * Desc: Convert images to webp
 */
gulp.task("convertToWebp", () =>
  gulp
    .src(`${src}/images/*.{png,jpg}`)
    .pipe(imagemin())
    .pipe(webp())
    .pipe(extReplace(".webp"))
    .pipe(gulp.dest(`${dist}/images`))
);

/**
 * Name: yamlToJson
 * Desc: Convert yaml to json
 */
gulp.task("yamlToJson", (done) => {
  gulp
    .src(`${src}/config/data.yaml`)
    .pipe(yaml({ space: 2 }))
    .pipe(gulp.dest(`${src}/config/`));
  done();
});

/**
 * Name: nunjucks
 * Desc: Render html files with Nunjucks and compress
 */
gulp.task("nunjucks", () =>
  gulp
    .src(`${src}/pages/*.njk`)
    .pipe(data(() => JSON.parse(fs.readFileSync(`${src}/config/data.json`))))
    .pipe(render({ path: [`${src}/templates`], watch: true }))
    .pipe(htmlmin({ collapseWhitespace: true, removeComments: true }))
    .pipe(gulp.dest(dist))
    .pipe(
      browserSync.reload({
        stream: true,
      })
    )
);

/**
 * Name: htmlmin
 * Desc: Compress html
 */
gulp.task("htmlmin", () =>
  gulp
    .src([`${dist}/*.html`])
    .pipe(htmlmin({ collapseWhitespace: true, removeComments: true }))
    .pipe(gulp.dest(dist))
);

/**
 * Name: Sync browser
 * Desc: When detect changes browser reload
 */
gulp.task("browserSync", () =>
  browserSync.init({
    watch: true,
    open: false,
    notify: false,
    logFileChanges: false,
    logLevel: "info",
    injectChanges: true,
    reloadOnRestart: true,
    server: {
      baseDir: dist,
    },
    files: [dist],
  })
);

/**
 * Name: Watch files
 * Desc: Watch css,js & nunjucks
 */
gulp.task("watchfiles", (done) => {
  gulp.watch(
    [`${src}/sass/**/*.scss`, `${src}/sass/styles.scss`],
    gulp.series("sass")
  );
  gulp.watch([`${src}/javascript/index.js`], gulp.series("javascript"));
  gulp.watch([`${src}/config/data.yaml`], gulp.series("yamlToJson"));
  gulp.watch(
    [`${src}/pages/*.njk`, `${src}/templates`, `${src}/config/*.json`],
    gulp.series("nunjucks")
  );
  gulp.watch([`${src}/images`], gulp.series("convertToWebp"));
  browserSync.reload();
  done();
});

/**
 * Name: Dev
 * Desc: Run gulp with live-serverls
 */
gulp.task(
  "default",
  gulp.series(
    "clean",
    "copyHtaccess",
    "copyRobots",
    "copyHumans",
    "copyIcons",
    "yamlToJson",
    "convertToWebp",
    "sass",
    "javascript",
    "nunjucks",
    "htmlmin",
    "watchfiles",
    "browserSync"
  )
);

/**
 * Name: Clean folders
 * Desc: Render tasks without live-server
 */
gulp.task(
  "public",
  gulp.series(
    "yamlToJson",
    "clean",
    "copyHtaccess",
    "copyRobots",
    "copyHumans",
    "copyIcons",
    "convertToWebp",
    "sass",
    "javascript",
    "nunjucks",
    "htmlmin",
    "cleanJson"
  )
);
