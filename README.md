# Instalación

Podemos usar Npm o Yarn yo prefiero Yarn pero para gustos.

    npm install o yarn install

Luego escribimos en el terminal `gulp` o `npm run start` o `yarn start`


# Enlaces

[Gulp](https://gulpjs.com/)
[Nunjucks](https://mozilla.github.io/nunjucks/)
[Sass](https://sass-lang.com/)

**Imagen:** [Piskel](https://www.piskelapp.com/)